#!/usr/bin/env python3

import sys
from datetime import datetime

s = sys.stdin.read().strip()
d = None

try:
	d = datetime.strptime(s, '%Y-%m-%d %H:%M:%S %z')
except:
	pass

try:
	d = datetime.strptime(s, '%Y-%m-%dT%H:%M:%S%Z')
except:
	pass

try:
	d = datetime.strptime(s, '%c')
except:
	pass

if d is not None:
	print(int(d.timestamp()))
